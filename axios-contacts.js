import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-some-tests.firebaseio.com/'
});

export default instance;