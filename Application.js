import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";
import {getContacts} from "./src/store/actions";
import Modal from "./src/components/Modal";

class Application extends Component {
  componentDidMount() {
    this.props.loadContent();
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          {this.props.contacts ? Object.keys(this.props.contacts).map(id => (
            <Modal key={id} id={id} info={this.props.contacts[id]}/>
          )) : null}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingHorizontal: 10
  }
});

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loadContent: () => dispatch(getContacts())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Application);