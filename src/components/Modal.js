import React, {Component} from 'react';
import {Modal, Text, TouchableOpacity, View, StyleSheet, Image, Button} from 'react-native';

class ModalContact extends Component {
  state = {
    modalVisible: false
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View style={{marginTop: 15}}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={styles.modal}>
            <View style={styles.modalItem}>
              <Text>{this.props.info.name}</Text>
              <Image resizeMode="contain" source={{uri: this.props.info.url}} style={styles.image} />
              <Text>{this.props.info.tel}</Text>
              <Text>{this.props.info.email}</Text>
            </View>
            <TouchableOpacity
              style={styles.backBtn}
              onPress={() => {
                this.setModalVisible(!this.state.modalVisible);
              }}>
              <View>
                <Text>Back to list</Text>
              </View>
            </TouchableOpacity>
          </View>
        </Modal>

        <TouchableOpacity
          onPress={() => {
            this.setModalVisible(true);
          }}>
          <View style={styles.item}>
            <Image resizeMode="contain" source={{uri: this.props.info.url}} style={styles.image} />
            <Text>{this.props.info.name}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1
  },
  modalItem: {
    width: 200,
    height: 200,
    padding: 20
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10,
    paddingRight: 70,
    borderRadius: 10
  },
  image: {
    width: 50,
    height: 50,
    marginRight: 10,
    borderRadius: 10
  },
  backBtn: {
    padding: 10,
    backgroundColor: 'blue'
  }
});

export default ModalContact;