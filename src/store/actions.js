import axios from '../../axios-contacts';

export const LOAD_CONTACTS = 'LOAD_CONTACTS';

export const loadContacts = contacts => {
  return {type: LOAD_CONTACTS, contacts};
};

export const getContacts = () => {
  return dispatch => {
    axios.get('contacts.json').then(response => {
      dispatch(loadContacts(response.data));
    }, error => {
      alert(error);
    });
  }
};